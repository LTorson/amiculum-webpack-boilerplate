require('offline-plugin/runtime').install();

import './fonts/libre-baskerville-v5-latin-regular.woff';
import './fonts/libre-baskerville-v5-latin-regular.woff2';

import './html/index.pug';
import './html/about-us.pug';
import './html/html.pug';

import './index.html';
import './index.scss';
import './scripts/script.js';